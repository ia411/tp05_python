NB_TOWER = 3

initial_state = [[1,2,3,4,5,6,7,8],[],[]]
final_state = [[],[],[1,2,3,4,5,6,7,8]]

def nextStates(state):
    """Return all possible states considerating the parameter state as the actual state.

    Call move for each tower 

    IN :
    state -- the actual state

    OUT :
    list of all possible next state.

    """
    next_states = []
    for tower_index in range(0, NB_TOWER):
        for new_state in move(state, tower_index):
            next_states.append(new_state)
    return next_states

def move(state, tower_index):
    """Return the list of state where we moved the first disk of the tower at index tower_index in
    the state in parameter.

    This function return only the valid state so it can be empty.

    IN :
    state -- the actual state
    tower_index -- index of the tower which we moved the first item

    OUT :
    The list of state where the first disk of the state has been moved.

    """
    next_states = []
    if(state[tower_index]):
        for index in range(0, NB_TOWER):
            if(index != tower_index and ((state[index] and state[index][0] > state[tower_index][0]) or not state[index])):
                new_state = copy_state(state)
                new_state[index].insert(0, new_state[tower_index].pop(0))
                next_states.append(new_state)
    return next_states

def copy_state(state):
    """Return a copy of the state in parameter.

    IN :
    state - the state to copy

    OUT :
    copied_state - a copy of the state

    """
    copied_state = [[], [], []]
    index = 0
    for sub_state in state:
        copied_state[index] = sub_state.copy()
        index += 1
    return copied_state

def bfs(start, final):
    """Application of the Breadth-First Search algorithm.

    It is the same as the precedent function except we create a list named closed to save the nodes to avoid adding multiple identical nodes.

    IN :
    start - initial state
    final - final state

    OUT :
    display of the number of nodes covered and the solution path.

    """
    node_number = 0
    queue = [[start, node_number, -1]]
    path = [[start, node_number, -1]]
    while queue and queue[0][0] != final:
        precedent_node = queue[0][1]
        for state in nextStates(queue.pop(0)[0]):
            node_number += 1
            queue.append([state, node_number, precedent_node])
            path.append([state, node_number, precedent_node])
    if(queue and queue[0][0] == final):
        path.append(queue[0])
        return "BFS : {0} noeuds parcourus pour trouver la solution suivante mesurant {1} noeuds :\n{2}".format(node_number, len(findPath(path)), findPath(path))
    return "BFS : pas de solution trouvée"

def bfs_occurence(start, final):
    """Application of the Breadth-First Search algorithm looking at the occurrences of the nodes.

    It is the same as the precedent function except we create a list named closed to save the nodes to avoid adding multiple identical nodes.

    IN :
    start - initial state
    final - final state

    OUT :
    display of the number of nodes covered and the solution path.

    """
    node_number = 0
    queue = [[start, node_number, -1]]
    path = [[start, node_number, -1]]
    while queue and queue[0][0] != final:
        precedent_node = queue[0][1]
        for state in nextStates(queue.pop(0)[0]):
            if (not contains(path, state)):
                node_number += 1
                queue.append([state, node_number, precedent_node])
                path.append([state, node_number, precedent_node])
    if(queue and queue[0][0] == final):
        path.append(queue[0])
        return "BFS occurence : {0} noeuds parcourus pour trouver la solution suivante mesurant {1} noeuds :\n{2}".format(node_number, len(findPath(path)), findPath(path))
    return "BFS occurence : pas de solution trouvée"

def dfs(start, final):  
    """Application of the Deep-First Search algorithm.

    It is the same as the precedent function except we create a list named closed to save the nodes to avoid adding multiple identical nodes.

    IN :
    start - initial state
    final - final state

    OUT :
    display of the number of nodes covered and the solution path.

    """
    node_number = 0
    queue = [[start, node_number, -1]]
    path = [[start, node_number, -1]]
    while queue and queue[0][0] != final:
        precedent_node = queue[0][1]
        for state in nextStates(queue.pop(0)[0]):
            node_number += 1
            queue.insert(0, [state, node_number, precedent_node])
            path.append([state, node_number, precedent_node])
    if(queue and queue[0][0] == final):
        path.append(queue[0])
        return "DFS : {0} noeuds parcourus pour trouver la solution suivante mesurant {1} noeuds :\n{2}".format(node_number, len(findPath(path)), findPath(path))
    return "DFS : pas de solution trouvée"

def dfs_occurence(start, final):
    """Application of the Deep-First Search algorithm looking at the occurrences of the nodes.

    It is the same as the precedent function except we create a list named closed to save the nodes to avoid adding multiple identical nodes.

    IN :
    start - initial state
    final - final state

    OUT :
    display of the number of nodes covered and the solution path.

    """
    node_number = 0
    queue = [[start, node_number, -1]]
    path = [[start, node_number, -1]]
    while queue and queue[0][0] != final:
        precedent_node = queue[0][1]
        for state in nextStates(queue.pop(0)[0]):
            if (not contains(path, state)):
                node_number += 1
                queue.insert(0, [state, node_number, precedent_node])
                path.append([state, node_number, precedent_node])
    if(queue and queue[0][0] == final):
        path.append(queue[0])
        return "DFS occurence : {0} noeuds parcourus pour trouver la solution suivante mesurant {1} noeuds :\n{2}".format(node_number, len(findPath(path)), findPath(path))
    return "DFS occurence : pas de solution trouvée"

def findPath(queue):
    """Function to use after running DFS or BFS to recreate the solution path.

    The only parameter is a list of state associated with their node number and father node number.
    Each element of of the list should look like this [state, node_number, precedent_node]. This format
    allow us to find each node's father starting from the last item of the list which is the goals node the 
    algorythm succed to reached.

    IN :
    queue - list of all the nodes covered by the algorythme. The final state is the last item of the list.
    OUT :
    return list of state discribing the solution path.

    """
    path = [queue[-1]]
    while (path[-1][1] != 0):
        for state in queue:
            if (path[-1][2] == state[1]):
                path.append(state)
                break
    ordered_path = []
    for state in path:
        ordered_path.insert(0, state[0])
    return ordered_path

def contains(closed, state):
    """Check if a state is in the closed list.

    Item in liste should look like this [state, node_number, precedent_node]. We compare only the states.

    IN :
    queue - list of all the nodes covered by the algorythme. The final state is the last item of the list.
    OUT :
    return True if the state is already in the list and False if not.
    """
    for node in closed:
        if (node[0] == state):
            return True
    return False

if __name__ == "__main__":
    # print(bfs(initial_state, final_state))
    print(bfs_occurence(initial_state, final_state))
    # print(dfs(initial_state, final_state))
    print(dfs_occurence(initial_state, final_state))