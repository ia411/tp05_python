import unittest
from exo2 import nextStates

class TestExo1(unittest.TestCase):

    def setUp(self):
        pass

    def testNextStates(self):
        test_list = nextStates(0)
        self.assertEqual(test_list, [8,1])
        test_list = nextStates(18)
        self.assertEqual(test_list, [17,19])
        test_list = nextStates(30)
        self.assertEqual(test_list, [38,22])
        test_list = nextStates(54)
        self.assertEqual(test_list, [62])
        test_list = nextStates(6)
        self.assertEqual(test_list, [5,7])

    def tearDown(self):
        pass