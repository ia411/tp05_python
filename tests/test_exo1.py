import unittest
from exo1 import move, nextStates

class TestExo1(unittest.TestCase):

    def setUp(self):
        pass

    def test_move(self):
        test_state = move([[1,2,3,4,5,6,7,8],[],[]], 0)
        self.assertEqual(test_state, [[[2, 3, 4, 5, 6, 7, 8], [1], []], [[2, 3, 4, 5, 6, 7, 8], [], [1]]])

        test_state = move([[2, 3, 4, 5, 6, 7, 8], [1], []], 1)
        self.assertEqual(test_state, [[[1, 2, 3, 4, 5, 6, 7, 8], [], []], [[2, 3, 4, 5, 6, 7, 8], [], [1]]])

        test_state = move([[2, 4, 5, 6, 7, 8], [1], [3]], 2)
        self.assertEqual(test_state, [])

    def test_nextState(self):
        test_states = nextStates([[1,2,3,4,5,6,7,8],[],[]])
        self.assertEqual(test_states, [[[2, 3, 4, 5, 6, 7, 8], [1], []], [[2, 3, 4, 5, 6, 7, 8], [], [1]]])

        test_states = nextStates([[2, 3, 4, 5, 6, 7, 8], [1], []])
        self.assertEqual(test_states, [[[3, 4, 5, 6, 7, 8], [1], [2]], [[1, 2, 3, 4, 5, 6, 7, 8], [], []], [[2, 3, 4, 5, 6, 7, 8], [], [1]]])
        
        test_states = nextStates([[2, 4, 5, 6, 7, 8], [1], [3]])
        self.assertEqual(test_states, [[[4, 5, 6, 7, 8], [1], [2, 3]], [[1, 2, 4, 5, 6, 7, 8], [], [3]], [[2, 4, 5, 6, 7, 8], [], [1, 3]]])

    def tearDown(self):
        pass